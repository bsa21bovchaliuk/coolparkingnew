﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking.BL.Models
{
    static class ValidatorForVehicle
    {
        private static readonly string pattern = @"\b[A-Z]{2}-\d{4}-[A-Z]{2}\b";

        public static void ValidateVehicleType(VehicleType vehicleType)
        {
            if (!Enum.IsDefined(typeof(VehicleType), vehicleType))
            {
                throw new ArgumentException("Invalid Vehicle Type!");
            }
        }

        public static void ValidateId(string id)
        {
            if (!Regex.IsMatch(id, pattern))
            {
                throw new ArgumentException("Incorrect Vehicle Id format! Correct Id format example: \"DV-2345-KJ\"");
            }
        }

        public static void ValidateBalance(decimal balance)
        {
            if (balance < 0)
            {
                throw new ArgumentException("Balance cannot be a negative value!");
            }
        }
    }
}
