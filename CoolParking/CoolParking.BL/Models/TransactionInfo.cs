﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using CoolParking.BL.Services;
using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public decimal Sum { get; }
        public string VehicleId { get; }
        public DateTime Time { get; }
        private readonly ParkingService parkingService;
        public TransactionInfo(string vehicleId, ParkingService _parkingService)
        {
            VehicleId = vehicleId;
            Time = DateTime.Now;
            parkingService = _parkingService;
            Sum = Calculate();
        }
        private decimal Calculate()
        {
            var vehicle = parkingService.FindVehicleForId(VehicleId);
            var tarif = parkingService.GetTarrifs()[vehicle.VehicleType];
            var fineRatio = parkingService.GetFineRatio();
            if (vehicle.Balance >= tarif)
            {
                return tarif;
            }
            else if ((vehicle.Balance >= 0))
            {
                if((vehicle.Balance + (tarif - vehicle.Balance) * fineRatio) < tarif)
                {
                    return tarif * fineRatio;
                }
            }
            return tarif;
        }

        public override string ToString()
        {
            return string.Format("[{0:HH:mm:ss}]  Vehicle id = \"{1}\"  withdrawing {2}", Time, VehicleId, Sum);
        }
    }
}
