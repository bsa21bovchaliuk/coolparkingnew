﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Linq;
using System.Text;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            ValidatorForVehicle.ValidateId(id);
            ValidatorForVehicle.ValidateVehicleType(vehicleType);
            ValidatorForVehicle.ValidateBalance(balance);

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
        public Vehicle(VehicleType vehicleType, decimal balance)
            : this(GenerateRandomRegistrationPlateNumber(), vehicleType, balance) { }
        
        public static string GenerateRandomRegistrationPlateNumber()
        {
            var result = new StringBuilder(10);

            for(int i = 0; i < 2; i++)
            {
                result.Append(RandomString(1));
            }

            result.Append('-');

            for(int i = 0; i < 2; i++)
            {
                result.Append(RandomNumber(2));
            }
            
            result.Append('-');

            for(int i = 0; i < 2; i++)
            {
                result.Append(RandomString(1));
            }

            return result.ToString();

            
        }
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
            .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public static string RandomNumber(int length)
        {
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length)
            .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        
        public override string ToString()
        {
            return String.Format("\"{0}\"  {1}  balance = {2}", Id.ToString(), VehicleType.ToString(), Balance.ToString());
        }
    }
}