﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System.IO;
using System;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }
        public LogService(string logPatch)
        {
            LogPath = logPatch;
        }
        public string Read()
        {
            if (!File.Exists(LogPath))
            {
                throw new InvalidOperationException("File not found");
            }
            return File.ReadAllText(LogPath);
        }

        public void Write(string logInfo)
        {
            using var streamWrite = File.AppendText(LogPath);
            streamWrite.WriteLine(logInfo);
        }
    }
}